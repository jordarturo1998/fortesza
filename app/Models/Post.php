<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'description',
        'status',
        'file',
        'price'
    ];

    protected $with = [
        "Messages"
    ];

    public function User(){
        return $this->belongsTo(User::class);
    }

    public function Messages(){
        return $this->hasMany(Message::class);
    }
 
}
