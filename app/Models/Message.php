<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $fillable = [
        'post_id',
        'anfitrion_id',
        'other_user_id',
        'text',
        'file',
        'status',
        'message_by',
        'created_at',
        'updated_at'
    ];

    public function Post(){
        return $this->belongsTo(Post::class);
    }

    public function AnfitrionUser() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function OtherUser() {
        return $this->belongsTo(User::class, 'user_id');
    }

}
