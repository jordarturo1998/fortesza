<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $pagination = 15;
    
        if($request->get('pagination')) {
            $pagination = $request->get('pagination');
        }
        
        $post = Post::orderBy("created_at", "asc")->paginate($pagination);
       
        return response()->json($post);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PostRequest $request)
    {

        $path = "post/user-". $request->user_id."/";
        $fileName = "";

        if ($request->hasFile('file')) {

            $file = $request->file("file");

            // Generate a file name with extension
            $fileName = $path.'file-'.time().'.'.$file->getClientOriginalExtension();

            Storage::disk('public')->put($fileName, "image");
        }

        $post = new Post();
        $post->user_id = $request->user_id;
        $post->description = $request->description;
        $post->status = $request->status;
        $post->price = $request->price;
        $post->file = $fileName ?? "";

        $post->save();

        return response()->json(["message" => "Post save successfull", "data" => $post]);

    }

    public function update(PostRequest $request, $id) {

        $path = "post/user-". $request->user_id."/";
        $fileName = "";

        $post = Post::find($id);

        if ($request->hasFile('file')) {

            if(!empty($post->file) && !is_null($post->file)) {
                $exist = Storage::disk("public")->exists($post->file);

                if($exist) {
                    Storage::disk('public')->delete($post->file);
                }
            }

            $file = $request->file("file");

            // Generate a file name with extension
            $fileName = $path.'file-'.time().'.'.$file->getClientOriginalExtension();

            Storage::disk('public')->put($fileName, "image");
        }

        $post->description = $request->description;
        $post->status = $request->status;
        $post->price = $request->price;
        $post->file = $fileName;

        $post->save();

        return response()->json(["message" => "Post update successfull", "post" => $post]);

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $post = Post::findOrFail($id);

        return response()->json($post);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $post = Post::findOrFail($id);

        if(!empty($post->file) && !is_null($post->file)) {
            $exist = Storage::disk("public")->exists($post->file);

            if($exist) {
                Storage::disk('public')->delete($post->file);
            }
        }

        $post->delete();

        return response()->json(["message" => "post deleted"]);
    }
}
