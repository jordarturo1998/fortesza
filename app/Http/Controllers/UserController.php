<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $pagination = 15;
    
        if($request->get('pagination')) {
            $pagination = $request->get('pagination');
        }
        
        $users = User::orderBy("created_at", "asc")->paginate($pagination);
       
        return response()->json($users);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $user = User::findOrFail($id);

        return response()->json($user);
    }
}
