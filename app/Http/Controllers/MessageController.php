<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageRequest;
use App\Http\Requests\StorePostRequest;
use App\Models\Message;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $pagination = 15;
    
        if($request->get('pagination')) {
            $pagination = $request->get('pagination');
        }
        
        $messages = Message::orderBy("created_at", "asc")->paginate($pagination);
        return response()->json($messages);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(MessageRequest $request)
    {

        $path = "messages/post-".$request->post_id."/user-". $request->message_by."/";
        $fileName = "";

        if ($request->hasFile('file')) {

            $file = $request->file("file");

            // Generate a file name with extension
            $fileName = $path.'file-'.time().'.'.$file->getClientOriginalExtension();

            Storage::disk('public')->put($fileName, "image");
        }

        $message = new Message();
        $message->post_id = $request->post_id;
        $message->anfitrion_id = $request->anfitrion_id;
        $message->other_user_id = $request->other_user_id;
        $message->text = $request->text;
        $message->status = $request->status;
        $message->message_by = $request->message_by;
        $message->file = $fileName ?? "";

        $message->save();

        return response()->json(["message" => "Message send successfull", "data" => $message]);

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $message = Message::findOrFail($id);

        return response()->json($message);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function update(MessageRequest $request, $id)
    {

        $path = "messages/post-".$request->post_id."/user-". $request->message_by."/";
        $fileName = "";

        $message = Message::find($id);    

        if ($request->hasFile('file')) {

            if(!empty($message->file) && !is_null($message->file)) {
                $exist = Storage::disk("public")->exists($message->file);

                if($exist) {
                    Storage::disk('public')->delete($message->file);
                }
            }

            $file = $request->file("file");

            // Generate a file name with extension
            $fileName = $path.'file-'.time().'.'.$file->getClientOriginalExtension();

            Storage::disk('public')->put($fileName, "image");
        }

        $message->post_id = $request->post_id;
        $message->anfitrion_id = $request->anfitrion_id;
        $message->other_user_id = $request->other_user_id;
        $message->text = $request->text;
        $message->status = $request->status;
        $message->message_by = $request->message_by;
        $message->file = $fileName ?? "";

        $message->save();

        return response()->json(["message" => "Message send successfull", "data" => $message]);

    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $message = Message::findOrFail($id);

        if(!empty($message->file) && !is_null($message->file)) {

            $exist = Storage::disk("public")->exists($message->file);

            if($exist) {
                Storage::disk('public')->delete($message->file);
            }
        }

        $message->delete();

        return response()->json(["message" => "message deleted"]);
    }
}
