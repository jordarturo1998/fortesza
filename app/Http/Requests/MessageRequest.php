<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $validator = [];
        switch ($this->method()) {
            case 'POST':
                if($this->hasFile('file')) {
                    $validator = [
                        'file'  =>  'required|file|image|mimes:jpeg,png,gif,jpg|max:2048',
                        'post_id' => 'required',
                        'anfitrion_id' => 'required',
                        'other_user_id' => 'required',
                        'text' => 'required',
                        'status' => 'required',
                        'message_by' => 'required'
                    ];
                } else {
                    $validator = [
                        'post_id' => 'required',
                        'anfitrion_id' => 'required',
                        'other_user_id' => 'required',
                        'text' => 'required',
                        'status' => 'required',
                        'message_by' => 'required'
                    ];
                }
                return $validator;
                break;
            case 'PUT':
                if($this->hasFile('file')) {
                    $validator = [
                        'file'  =>  'required|file|image|mimes:jpeg,png,gif,jpg|max:2048',
                        'post_id' => 'required',
                        'anfitrion_id' => 'required',
                        'other_user_id' => 'required',
                        'text' => 'required',
                        'status' => 'required',
                        'message_by' => 'required'
                    ];
                } else {
                    $validator = [
                        'post_id' => 'required',
                        'anfitrion_id' => 'required',
                        'other_user_id' => 'required',
                        'text' => 'required',
                        'status' => 'required',
                        'message_by' => 'required'
                    ];
                }
                return $validator;
                break;
        }
    }
}
