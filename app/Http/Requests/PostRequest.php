<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $validator = [];
        switch ($this->method()) {
            case 'POST':
                if($this->hasFile('image')) {
                    $validator = [
                        'image'  =>  'required|file|image|mimes:jpeg,png,gif,jpg|max:2048',
                        'user_id' => 'required',
                        'description' => 'required',
                        'status' => 'required',
                        'price' => 'required',
                    ];
                } else {
                    $validator = [
                        'user_id' => 'required',
                        'description' => 'required',
                        'status' => 'required',
                        'price' => 'required',
                    ];
                }
                return $validator;
                break;
            case 'PUT':
                if($this->hasFile('image')) {
                    $validator = [
                        'image'  =>  'required|file|image|mimes:jpeg,png,gif,jpg|max:2048',
                        'user_id' => 'required',
                        'description' => 'required',
                        'status' => 'required',
                        'price' => 'required',
                    ];
                } else {
                    $validator = [
                        'user_id' => 'required',
                        'description' => 'required',
                        'status' => 'required',
                        'price' => 'required',
                    ];
                }
                return $validator;
                break;
        }
      
    }
}
