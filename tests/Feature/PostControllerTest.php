<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostControllerTest extends TestCase
{

    use RefreshDatabase;

    public function test_get_posts(): void
    {
        $this->getJson('/api/posts')->assertStatus(200);
    }

    public function test_save_posts(): void
    {
        $user = User::factory()->create([
             'name' => fake()->name(),
            'username' => fake()->userName(),
            'email' =>  fake()->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => bcrypt('1234')
        ]);
        $body = [
            'user_id' => $user->id,
            'description' => "Aprende a programar",
            'status' => "active",
            'price' => "120.000"
        ];
        $this->postJson('/api/posts', $body)
            ->assertStatus(200)
                ->assertValid([
                    'message' => "Post save successfull",
                    'data' => [
                        'user_id' => "1",
                        'description' => "Aprende ingles básico",
                        'status' => "active",
                        'price' => "120.000"
                    ]
                ]);
    }


    public function test_update_post_by_id(): void
    {
        $user = User::factory()->create([
             'name' => fake()->name(),
            'username' => fake()->userName(),
            'email' =>  fake()->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => bcrypt('1234')
        ]);

        $post = Post::factory()->create([
            'user_id' =>  $user->id,
            'description' => "Aprende ingles básico",
            'status' => "active",
            'price' => "120.000"
        ]);

        $body = [
            'user_id' => $user->id,
            'description' => "Aprende a programar",
            'status' => "active",
            'price' => "90.000"
        ];

        $this->putJson('/api/posts/'.$post->id, $body)
            ->assertStatus(200)
            ->assertValid([
                "message" => "Post update successfull", "post" => $body
            ]);
    }

    public function test_find_post_by_id(): void
    {
        $user = User::factory()->create([
            'name' => fake()->name(),
            'username' => fake()->userName(),
            'email' =>  fake()->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => bcrypt('1234')
        ]);

        $post = Post::factory()->create([
            'user_id' => $user->id,
            'description' => "Aprende a programar",
            'status' => "active",
            'price' => "120.000"
        ]);
        $this->getJson('/api/posts/'.$post->id)
            ->assertStatus(200)
                ->assertValid([
                    'id' => $post->id,  
                    'user_id' => "1",
                    'description' => "Aprende a programar",
                    'status' => "active",
                    'price' => "120.000"
                ]);
    }

    public function test_delete_post_by_id(): void
    {
        $user = User::factory()->create([
            'name' => fake()->name(),
            'username' => fake()->userName(),
            'email' =>  fake()->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => bcrypt('1234')
        ]);

        $post = Post::factory()->create([
             'user_id' => $user->id,
            'description' => "Aprende a programar",
            'status' => "active",
        ]);
        $this->deleteJson('/api/posts/'.$post->id)
            ->assertStatus(200
            )->assertJson(["message" => "post deleted"]);
    }
}
