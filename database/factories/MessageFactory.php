<?php

namespace Database\Factories;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Message>
 */
class MessageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $anfitrion = User::all()->random()->id;
        $other = User::all()->random()->id;
        $other = $other == $anfitrion ? User::all()->random()->id : $other;
        $post = Post::all()->random()->id;

        return [
            'post_id' => $post,
            'anfitrion_id' => $anfitrion,
            'other_user_id' => $other,
            'text' => fake()->text(),
            'message_by' => $other,
            'status' => "viewed"
        ];
    }
}
